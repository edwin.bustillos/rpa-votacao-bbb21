var robot = require("robotjs");

// Speed up the mouse.
robot.setMouseDelay(2);

var twoPI = Math.PI * 2.0;
var screenSize = robot.getScreenSize();
var height = (screenSize.height / 2) - 10;
var width = (screenSize.width / 2) - 10;

var e = 0;  
for (let i = 0; i < 10; i++) {
    e++
    setTimeout(() => {
        console.log(i)
        robot.moveMouse(width, height);
        robot.mouseClick();
    }, 1000*e);
    e++
    setTimeout(() => {
        console.log(i)
        robot.moveMouse(width, height-150);
        robot.mouseClick();
    }, 1000*e);
}